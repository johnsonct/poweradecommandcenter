#!/bin/bash


# this script starts chrome on boot and opens the command center app

DISPLAY=:0 /usr/bin/chromium-browser --no-sandbox --remote-debugging-port=8888 --ignore-gpu-blacklist --disable-pinch --overscroll-history-navigation=0 --disable-infobars --disable-session-crashed-bubble --disable-session-restore --allow-file-access-from-files --window-size=192,32 --window-position=0,0 --no-first-run --kiosk file:///home/pi/powerade/touchHeaderBoard/touchVideo.html
