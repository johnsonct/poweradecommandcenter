class EventSocket {
  constructor(url) {
    this.eventListeners = {}
    this.sendQueue = []
    this.isConnected = false
    this.isReconnecting = false
    this.reconnectAttempts = 0
    this.init(url)
  }

  init(url) {
    this.websocket = new WebSocket(url)

    this.websocket.onopen = this.handleOpen.bind(this)

    this.websocket.onmessage = this.handleMessage.bind(this)

    this.websocket.onclose = this.handleClose.bind(this)
  }

  handleOpen() {
    console.log('EventSocket connected')
    if (this.isReconnecting) window.location.reload()
    else {
      this.isConnected = true
      this.reconnectAttempts = 0
      if (this.reconnectInterval) clearInterval(this.reconnectInterval)
      while (this.sendQueue && this.sendQueue.length > 0) {
        this.websocket.send(this.sendQueue.pop())
      }
    }
  }

  /*
    Example websocket message:
    `topic:/dispenser/broker/session
    topic:/dispenser/broker/test

    {"sessionId":4}`
  */
  parseSocketMessage(data) {
    console.log(data)
    const [headersString, msg] = data.split(/\n\n/)
    const headers = headersString.split(/\n/)
    return [headers, JSON.parse(msg)]
  }

  handleMessage(evt) {
    const [ headers, data ] = this.parseSocketMessage(evt.data)
    const msgType = 'powerade'

    // if (msgType) console.log('msgType: ', msgType)

    if (headers && headers[0] === 'topic:/dispenser/broker/session') {
      console.log('Established session ID:', data.sessionId)
    }
    else if (this.eventListeners[msgType]) {
      this.eventListeners[msgType].forEach(listener => {
        try {
          listener(data)
        } catch (error) {
          console.log(error)
        }
      })
    } else {
      console.log('No way to handle event socket message with path: ', msgType, '\n',
      'Full socket event:', data)
    }
  }

  handleClose() {
    let attempts = this.reconnectAttempts + 1
    const time = Math.min(30, (attempts * 2)) * 1000
    console.log(`EventSocket disconnected, attempting reconnect in ${time}ms (try #${attempts})`)
    this.isReconnecting = true

    this.reconnectInterval = setTimeout(() => {
      this.reconnectAttempts += 1
      this.init()
    }, time)
  }

  addListener(msgPath, msgType, listener) {
    this.eventListeners[msgType] = this.eventListeners[msgType] || []
    this.eventListeners[msgType].push(listener)

    this.send(`subscribe:${msgPath}`)
  }

  removeListener(msgPath, listener) {
    if (this.eventListeners[msgPath]) {
      const index = this.eventListeners[msgPath].indexOf(listener)

      if (index >= 0) {
        this.eventListeners[msgPath].splice(index, 1)
        this.eventListeners[msgPath].length === 0 && delete this.eventListeners[msgPath]
      }
    }
  }

  send(data) {
    if (!this.isConnected) {
      this.sendQueue.push(data)
    }
    else {
      console.log('send', data)
      this.websocket.send(data)
    }
  }
}
