#!/bin/bash
# this script will play the video in the top display...only. It needs X to already be running..

###### resetting chrome so the flag will not show

CHROMIUM_PREFS_FILE=/root/.config/chromium/Default/Preferences
CHROMIUM_LOCAL_STATE=/root/.config/chromium/'Local State'

#########################################
# Reset Chromium Crash Settings
#########################################
if [ -e "$CHROMIUM_PREFS_FILE" ]; then
	  sed -i 's/"exited_cleanly":false/"exited_cleanly":true/' "$CHROMIUM_LOCAL_STATE"
	    sed -i 's/"exited_cleanly":false/"exited_cleanly":true/; s/"exit_type":"[^"]\+"/"exit_type":"Normal"/' "$CHROMIUM_PREFS_FILE"
    fi

    exit 0

#### showing the video
.
DISPLAY=:0 /usr/bin/chromium-browser --no-sandbox --remote-debugging-port=8888 --ignore-gpu-blacklist --disable-pinch --overscroll-history-navigation=0 --disable-infobars --disable-session-crashed-bubble --disable-session-restore --allow-file-access-from-files --window-size=1920,1880 --window-position=0,0 --no-first-run --kiosk file:///home/pi/powerade/loopHeaderBoard/loopVideo.html &
