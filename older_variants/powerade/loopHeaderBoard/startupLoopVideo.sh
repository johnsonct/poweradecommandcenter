#!/bin/bash


# start X11
Xorg :0 -s 0 +iglx -nocursor -xinerama -wr &

# this script starts chrome on boot and opens the command center app
# starting in incognito mode to avoid the restore flag
DISPLAY=:0 /usr/bin/chromium-browser --incognito --no-sandbox --remote-debugging-port=8888 --ignore-gpu-blacklist --disable-pinch --overscroll-history-navigation=0 --disable-infobars --disable-session-crashed-bubble --disable-session-restore --allow-file-access-from-files --window-size=1920,1880 --window-position=0,0 --no-first-run --kiosk file:///home/pi/powerade/loopHeaderBoard/loopVideo.html &
